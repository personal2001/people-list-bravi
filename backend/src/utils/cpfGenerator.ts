const random = () => ("" + Math.floor(Math.random() * 999)).padStart(3, '0');

function dig(n1: string, n2: string, n3: string, n4?: number) {
    const nums = n1.split("").concat(n2.split(""), n3.split(""));
    if (n4 !== undefined){
        nums[9] = String(n4);
    }
    let x = 0;
    for (let i = (n4 !== undefined ? 11 : 10), j = 0; i >= 2; i--, j++) x += +nums[j] * i;
    const y = x % 11;
    return y < 2 ? 0 : 11 - y;
}

export default function CPF() {
    const n1 = random(), n2 = random(), n3 = random(), d1 = dig(n1, n2, n3);
    return `${n1}${n2}${n3}${d1}${dig(n1, n2, n3, d1)}`;
}
