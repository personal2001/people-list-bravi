import { IsMongoId } from 'class-validator';

/**
 * Check if param id is an ObjectId
 */
class ParamsWithId {
    @IsMongoId()
    id: string;
}

export default ParamsWithId;