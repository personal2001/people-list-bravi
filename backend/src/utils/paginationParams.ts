import { Type } from 'class-transformer';
import { IsNumber, Min, IsOptional } from 'class-validator';

/**
 * Class for paginate
 */
export class PaginationParams {
    @IsOptional()
    @Type(() => Number)
    @IsNumber()
    @Min(0)
    skip?: number;

    @IsOptional()
    @Type(() => Number)
    @IsNumber()
    @Min(1)
    limit?: number;
}