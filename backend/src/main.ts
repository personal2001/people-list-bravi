import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import {NestExpressApplication} from "@nestjs/platform-express";
import * as requestIp from 'request-ip';
import * as helmet from 'helmet';

import { AppModule } from './app.module';
// @ts-ignore
import getLogLevels from "./core/logs/getLogLevels";
import {AllExceptionsFilter} from "./core/exceptions/exceptionFilter";
import {CorsInterceptor} from "./core/cors/cors.interceptor";

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    logger: getLogLevels(process.env.NODE_ENV === 'PROD'),
  });

  /**
   * Global Prefix of API
   */
  app.setGlobalPrefix('api');

  /**
   * Pipe for validade incoming data
   */
  app.useGlobalPipes(new ValidationPipe());


  /**
   * Global custom exception filter
   */
  app.useGlobalFilters(new AllExceptionsFilter());

  /**
   * CORS configuration
   */
  app.enableCors({
    origin: /https?:\/\/(([^\/]+\.)?(vercel|heroku|localhost)(\.|:)?(app|com|3001|4200)?\.?(br)?)$/i,
    methods: 'GET, PUT, POST, DELETE, OPTIONS',
    allowedHeaders:
        'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, Authorization',
    credentials: true,
    maxAge: 86400,
    preflightContinue: false,
    optionsSuccessStatus: 204,
  });

  /**
   * Helmet configuration for extra security on headers
   */
  app.use(helmet.contentSecurityPolicy());
  app.use(helmet.crossOriginEmbedderPolicy());
  app.use(helmet.crossOriginOpenerPolicy());
  app.use(helmet.crossOriginResourcePolicy());
  app.use(helmet.dnsPrefetchControl());
  app.use(helmet.expectCt());
  app.use(helmet.frameguard());
  app.use(helmet.hidePoweredBy());
  app.use(helmet.hsts());
  app.use(helmet.ieNoOpen());
  app.use(helmet.noSniff());
  app.use(helmet.originAgentCluster());
  app.use(helmet.permittedCrossDomainPolicies());
  app.use(helmet.referrerPolicy());
  app.use(helmet.xssFilter());

  /**
   * Extract request ip
   */
  app.use(requestIp.mw());

  /**
   * Interceptor for caching pre-flight OPTIONS request
   */
  app.useGlobalInterceptors(new CorsInterceptor());

  await app.listen(process.env.PORT || 3000);
}
bootstrap();
