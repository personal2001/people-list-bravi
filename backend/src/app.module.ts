import {Module, MiddlewareConsumer, RequestMethod} from '@nestjs/common';

import PeoplesModule from './api/peoples/peoples.module';
import {DatabaseModule} from "./database/database.module";
import {ConfigModule} from "./core/config/config.module";
// @ts-ignore
import LogsMiddleware from "./core/logs/logs.middleware";
import HealthModule from "./api/health/health.module";
// @ts-ignore
import LogsModule from "./api/logs/logs.module";

@Module({
  imports: [
    ConfigModule,
    DatabaseModule,
    PeoplesModule,
    HealthModule,
    LogsModule,
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LogsMiddleware).forRoutes({
      path:'*',
      method: RequestMethod.ALL
    });
  }
}
