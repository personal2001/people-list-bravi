import {Injectable, Logger, NestMiddleware} from '@nestjs/common';
import {Request, Response, NextFunction} from 'express';

import LogsService from "../../api/logs/logs.service";
import CreateLogDto from "../../api/logs/dto/create-contact.dto";

@Injectable()
class LogsMiddleware implements NestMiddleware {
    private readonly logger = new Logger('API');

    constructor(
        private readonly logsService: LogsService
    ) {
    }

    async use(request: Request, response: Response, next: NextFunction) {
        response.on('finish', async () => {
            const {method, originalUrl, clientIp} = request;
            const {statusCode, statusMessage} = response;

            const message = `${clientIp} ${method} ${originalUrl} ${statusCode} ${statusMessage}`;

            let  log: CreateLogDto = {
                code: String(statusCode), ip: String(clientIp),
                method: method, path: originalUrl,
                status: statusMessage, message: message,
                context: 'API',
                level: 'info'
            }

            if (statusCode >= 500) {
                log.level = 'error'
                await this.logsService.createLog(log)
                return this.logger.error(message);
            }

            if (statusCode >= 400) {
                log.level = 'warn'
                await this.logsService.createLog(log)
                return this.logger.warn(message);
            }

            await this.logsService.createLog(log)
            return this.logger.log(message);
        });
        next();
    }
}

export default LogsMiddleware;
