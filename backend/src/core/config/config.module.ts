import { Module } from '@nestjs/common';
import { ConfigModule as config } from '@nestjs/config';
import * as Joi from "@hapi/joi";

@Module({
    imports: [
        config.forRoot({
            validationSchema: Joi.object({
                MONGO_REPLICASET: Joi.string().required(),
                MONGO_REPLICA_HOST_1: Joi.string().required(),
                MONGO_REPLICA_PORT_1: Joi.string().required(),
                MONGO_REPLICA_HOST_2: Joi.string().required(),
                MONGO_REPLICA_PORT_2: Joi.string().required(),
                MONGO_REPLICA_HOST_3: Joi.string().required(),
                MONGO_REPLICA_PORT_3: Joi.string().required(),
            }),
        }),
    ],
})
export class ConfigModule {}