import {CallHandler, ExecutionContext, Injectable, Logger, NestInterceptor} from "@nestjs/common";
import {Observable} from "rxjs";
import {IncomingMessage, ServerResponse} from "http";

@Injectable()
export class CorsInterceptor implements NestInterceptor {
    private readonly logger = new Logger('API');

    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const request: IncomingMessage = context.switchToHttp().getRequest();
        const response: ServerResponse = context.switchToHttp().getResponse();

        if (request.method === 'OPTIONS') {
            this.logger.log('Setting Cache For Preflight');
            response.setHeader('Cache-Control', 'public, max-age=86400')
        }
        return next.handle();
    }
}