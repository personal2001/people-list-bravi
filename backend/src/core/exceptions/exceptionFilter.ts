import {
    ExceptionFilter,
    Catch,
    ArgumentsHost,
    HttpException,
    HttpStatus,
    InternalServerErrorException,
} from '@nestjs/common';

import {Error} from 'mongoose';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    catch(exception: InternalServerErrorException | any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const request = ctx.getRequest();
        const status = exception instanceof HttpException ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR;

        /**
         * @description Exception json response
         * @param type
         * @param errorMessage
         * @param _status
         * @param message
         */
        const responseMessage = (type: string, errorMessage: string, _status = status, message?: string) => {
            response.status(_status).json({
                statusCode: _status,
                path: request.url,
                errorType: type,
                errorMessage: errorMessage,
                message: message,
            });
        };

        // Throw an exceptions for either
        // MongoError, ValidationError, TypeError, CastError and Error
        if (exception instanceof Error.ValidationError) {
            responseMessage('Validation', exception.message, 422);
        } else if (exception instanceof Error.CastError) {
            responseMessage('Cast', exception.message, 422);
        } else if (exception instanceof Error.DocumentNotFoundError) {
            responseMessage('Not Found', exception.message, 404);
        } else if (exception.name == 'MongoServerError' || exception.name == 'MongoBulkWriteError') {
            if (exception.code === 11000) {
                let message;
                const collection = exception.message.split('collection')[1].split(' ')[1].split('.')[1];
                switch (collection) {
                    case 'peoples':
                        const {fiscalNumber} = exception.keyValue;
                        message = `the cpf ${fiscalNumber} is already in use`;
                        return responseMessage('MongoDupKey', message, 400);
                    default:
                        return responseMessage('MongoDupKey', exception.message, 400);
                }
            } else {
                responseMessage('MongoServerError', exception.message, 400);
            }
        } else {
            // @ts-ignore
            if (exception instanceof Error) {
                responseMessage('Error', exception.message, 400);
            } else if (status === 503) {
                response.status(status).json(exception.getResponse());
            } else {
                responseMessage(exception.name, exception.message, exception.status, exception?.response?.message);
            }
        }
    }
}
