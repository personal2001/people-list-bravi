import { CreatePeopleDto } from "../dto/create-people.dto";

export const CreatePeopleDtoMock = (): CreatePeopleDto => {
    return {
        birthdate: "1997-07-16",
        fiscalNumber: "70283854421",
        gender: "male",
        name: "Bruno Pereira",
        contacts: []
    };
};