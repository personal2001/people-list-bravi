import { Config, names, uniqueNamesGenerator } from 'unique-names-generator';
import { CreatePeopleDto } from "../dto/create-people.dto";
import CPF from "../../../utils/cpfGenerator";

export const PeopleDtoMock = (): CreatePeopleDto => {
    const name: Config = {
        dictionaries: [names],
        style: 'capital',
    };

    return {
        birthdate: "1997-07-16",
        fiscalNumber: CPF(),
        gender: "male",
        name: `${uniqueNamesGenerator(name)} ${uniqueNamesGenerator(name)}`,
        contacts: [],
    };
};