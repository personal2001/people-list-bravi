import { UpdatePeopleDto } from "../dto/update-people.dto";

export const UpdatePeopleDtoMock = (): UpdatePeopleDto => {
    return {
        birthdate: "1997-07-18",
        fiscalNumber: "70283854421",
        gender: "male",
        name: "Bruno Guilherme",
    };
};