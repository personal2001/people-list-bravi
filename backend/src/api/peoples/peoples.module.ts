import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import PeoplesController from './peoples.controller';
import { PeoplesService } from './peoples.service';
import { People, PeopleSchema } from './schema/people.schema';
import ContactsModule from "../contacts/contacts.module";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: People.name, schema: PeopleSchema }]),
    ContactsModule
  ],
  controllers: [PeoplesController],
  providers: [PeoplesService],
})
export default class PeoplesModule {}
