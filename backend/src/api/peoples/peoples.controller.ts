import {Controller, Get, Post, Body, Put, Param, Delete, Query} from '@nestjs/common';

import ParamsWithId from '../../utils/paramsWithId';
import { PeoplesService } from './peoples.service';
import { CreatePeopleDto } from './dto/create-people.dto';
import { UpdatePeopleDto } from './dto/update-people.dto';
import {PaginationParams} from "../../utils/paginationParams";

@Controller('peoples')
export default class PeoplesController {
  constructor(private readonly peoplesService: PeoplesService) {}

  /**
   *  This function call findAll service to return all peoples
   */
  @Get()
  async getAllPeoples(@Query() { skip, limit }: PaginationParams) {
    return this.peoplesService.findAll(skip, limit);
  }

  /**
   *  This function call findOne service to return an id of people
   */
  @Get(':id')
  async getPeople(@Param() { id }: ParamsWithId) {
    return this.peoplesService.findOne(id);
  }

  /**
   *  This function call create service to create a new people
   */
  @Post()
  async createPeople(@Body() createPeopleDto: CreatePeopleDto) {
    return this.peoplesService.create(createPeopleDto);
  }

  /**
   *  This function call update service to update a id of people
   */
  @Put(':id')
  updatePeople(@Param() { id }: ParamsWithId, @Body() updatePeopleDto: UpdatePeopleDto) {
    return this.peoplesService.update(id, updatePeopleDto);
  }

  /**
   *  This function call delete service to delete a id of people
   */
  @Delete(':id')
  deletePeople(@Param() { id }: ParamsWithId) {
    return this.peoplesService.delete(id);
  }
}
