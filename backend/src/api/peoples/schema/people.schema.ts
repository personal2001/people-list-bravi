import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {Document, ObjectId} from 'mongoose';
import {Transform} from "class-transformer";

export type PeopleDocument = People & Document

@Schema({
    timestamps: true,
})
export class People {
    @Transform(({ value }) => value.toString())
    _id: ObjectId;

    @Prop()
    name: string;

    @Prop()
    birthdate: Date;

    @Prop()
    gender: string;

    @Prop({unique: true})
    fiscalNumber: string;
}

const PeopleSchema = SchemaFactory.createForClass(People)

export { PeopleSchema };