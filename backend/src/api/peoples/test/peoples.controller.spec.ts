import {Test, TestingModule} from "@nestjs/testing";
import {INestApplication, ValidationPipe} from "@nestjs/common";
import {connect, Connection, Model} from "mongoose";
import {getModelToken} from "@nestjs/mongoose";
import {MongoMemoryReplSet} from "mongodb-memory-server";
import request from 'supertest';

import {People, PeopleSchema} from "../schema/people.schema";
import {PeoplesService} from "../peoples.service";
import PeoplesController from "../peoples.controller";
import {PeopleDtoMock} from "../mock/people.dto.mock";
import {ContactsService} from "../../contacts/contacts.service";
import {Contact, ContactSchema} from "../../contacts/schema/contact.schema";

describe('PeoplesController', () => {
    let app: INestApplication;
    let service: PeoplesService;
    let controller: PeoplesController;
    let mongo: MongoMemoryReplSet;
    let mongoConnection: Connection;
    let peopleModel: Model<People>;
    let contactModel: Model<Contact>;

    /**
     * Creating an mongodb in memory
     */
    beforeAll(async () => {
        mongo = await MongoMemoryReplSet.create();
        const uri = mongo.getUri();
        mongoConnection = (await connect(uri)).connection;
        peopleModel = mongoConnection.model(People.name, PeopleSchema);
        contactModel = mongoConnection.model(Contact.name, ContactSchema);
        const module: TestingModule = await Test.createTestingModule({
            controllers: [PeoplesController],
            providers: [
                PeoplesService,
                ContactsService,
                {provide: getModelToken(People.name), useValue: peopleModel},
                {provide: getModelToken(Contact.name), useValue: contactModel},
            ],
        }).compile();
        app = module.createNestApplication();
        app.useGlobalPipes(new ValidationPipe());
        await app.init();
        service = app.get<PeoplesService>(PeoplesService);
        controller = app.get<PeoplesController>(PeoplesController);
    });

    /**
     * Closing all connections and deleting database
     */
    afterAll(async () => {
        await mongoConnection.dropDatabase();
        await mongoConnection.close();
        await mongo.stop();
    });

    /**
     * Clear collections after test
     */
    afterEach(async () => {
        const collections = mongoConnection.collections;
        for (const key in collections) {
            const collection = collections[key];
            await collection.deleteMany({});
        }
    });

    /**
     * Verify if service and controller is defined
     */
    it('should be defined', () => {
        expect(service).toBeDefined();
        expect(controller).toBeDefined();
    });

    /**
     * Teste of createPeople controller of people
     */
    describe('createPeople', () => {
        it('should return the corresponding saved object', async() => {
            return request(app.getHttpServer())
                .post(`/peoples`)
                .send(PeopleDtoMock())
                .expect(201)
        })
        it('should return (Bad Request - 400)', async() => {
            return request(app.getHttpServer())
                .post('/peoples')
                .send()
                .expect(400)
        })
    })

    /**
     * Teste of getAllPeoples controller of people
     */
    describe('getAllPeoples', () => {
        it('should return the corresponding saved object', async() => {
            for (let i: number = 0; i < 10; i++) {
                await service.create(PeopleDtoMock())
            }
            return request(app.getHttpServer())
                .get(`/peoples`)
                .expect(200)
        })
        it('should return the corresponding saved object with pagination', async() => {
            for (let i: number = 0; i < 10; i++) {
                await service.create(PeopleDtoMock())
            }
            return request(app.getHttpServer())
                .get(`/peoples?skip=4&limit=2`)
                .expect(200)
        })
    })

    /**
     * Teste of getPeople controller of people
     */
    describe('getPeople', () => {
        it('should return the corresponding saved object', async() => {
            const people = await service.create(PeopleDtoMock())
            return request(app.getHttpServer())
                // @ts-ignore
                .get(`/peoples/${people._id}`)
                .expect(200)
        })
        it('should return (Not Found - 404)', async() => {
            return request(app.getHttpServer())
                .get('/peoples/63004e97c99ca7dabcf36341')
                .expect(404)
        })
        it('should return (Bad Request - 400)', async() => {
            return request(app.getHttpServer())
                .get('/peoples/1')
                .expect(400)
        })
    })

    /**
     * Teste of updatePeople controller of people
     */
    describe('updatePeople', () => {
        it('should return the corresponding saved object', async() => {
            const people = await service.create(PeopleDtoMock())
            return request(app.getHttpServer())
                // @ts-ignore
                .put(`/peoples/${people._id}`)
                .send({name: 'Bruno Pereira'})
                .expect(200)
        })
        it('should return (Not Found - 404)', async() => {
            return request(app.getHttpServer())
                .get('/peoples/63004e97c99ca7dabcf36341')
                .expect(404)
        })
        it('should return (Bad Request - 400)', async() => {
            return request(app.getHttpServer())
                .put(`/peoples/1`)
                .send({name: 'Bruno Pereira'})
                .expect(400)
        })
    })

    /**
     * Teste of deletePeople controller of people
     */
    describe('deletePeople', () => {
        it('should return null', async() => {
            const people = await service.create(PeopleDtoMock())
            return request(app.getHttpServer())
                // @ts-ignore
                .delete(`/peoples/${people._id}`)
                .expect(200)
        })
        it('should return (Not Found - 404)', async() => {
            return request(app.getHttpServer())
                .get('/peoples/63004e97c99ca7dabcf36341')
                .expect(404)
        })
        it('should return (Bad Request - 400)', async() => {
            return request(app.getHttpServer())
                .put(`/peoples/1`)
                .expect(400)
        })
    })

});
