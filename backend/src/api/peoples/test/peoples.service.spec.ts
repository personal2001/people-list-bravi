import {Test, TestingModule} from '@nestjs/testing';
import {connect, Connection, Model} from "mongoose";
import {MongoMemoryReplSet} from "mongodb-memory-server";

import {PeoplesService} from '../peoples.service';
import {People, PeopleDocument, PeopleSchema} from "../schema/people.schema";
import {getModelToken} from "@nestjs/mongoose";
import {CreatePeopleDtoMock} from "../mock/create-people.dto.mock";
import {UpdatePeopleDtoMock} from "../mock/update-people.dto.mock";
import {PeopleDtoMock} from "../mock/people.dto.mock";
import {ContactsService} from "../../contacts/contacts.service";
import {Contact, ContactSchema} from "../../contacts/schema/contact.schema";

describe('PeoplesService', () => {
    let service: PeoplesService;
    let mongo: MongoMemoryReplSet;
    let mongoConnection: Connection;
    let peopleModel: Model<People>;
    let contactModel: Model<Contact>;

    /**
     * Creating an mongodb in memory
     */
    beforeAll(async () => {
        mongo = await MongoMemoryReplSet.create();
        const uri = mongo.getUri();
        mongoConnection = (await connect(uri)).connection;
        peopleModel = mongoConnection.model(People.name, PeopleSchema);
        contactModel = mongoConnection.model(Contact.name, ContactSchema);
        const app: TestingModule = await Test.createTestingModule({
            providers: [
                PeoplesService,
                ContactsService,
                {provide: getModelToken(People.name), useValue: peopleModel},
                {provide: getModelToken(Contact.name), useValue: contactModel},
            ],
        }).compile();
        service = app.get<PeoplesService>(PeoplesService);
    });

    /**
     * Closing all connections and deleting database
     */
    afterAll(async () => {
        await mongoConnection.dropDatabase();
        await mongoConnection.close();
        await mongo.stop();
    });

    /**
     * Clear collections after test
     */
    afterEach(async () => {
        const collections = mongoConnection.collections;
        for (const key in collections) {
            const collection = collections[key];
            await collection.deleteMany({});
        }
    });

    /**
     * Verify if service is defined
     */
    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    /**
     * Teste of create service of people
     */
    describe('when creating people', () => {
        describe('and fiscalNumber does not exists', () => {
            it('should return people)', async () => {
                const createdPeople = await service.create(CreatePeopleDtoMock())
                // @ts-ignore
                expect(createdPeople.name).toBe(CreatePeopleDtoMock().name);
                // @ts-ignore
                expect(createdPeople.birthdate.toISOString().split('T')[0]).toBe(CreatePeopleDtoMock().birthdate);
                // @ts-ignore
                expect(createdPeople.gender).toBe(CreatePeopleDtoMock().gender);
                // @ts-ignore
                expect(createdPeople.fiscalNumber).toBe(CreatePeopleDtoMock().fiscalNumber);
            })
        })
        describe('and fiscalNumber exists', () => {
            it('should throw  E11000 duplicate key exception)', async () => {
                await (new peopleModel(CreatePeopleDtoMock()).save());
                await expect(service.create(CreatePeopleDtoMock())).rejects.toThrow()
            })
        })
    })

    /**
     * Teste of findAll service of people
     */
    describe('when getting all peoples', () => {
        describe('and the result is more than 0', () => {
            describe('and does not have pagination', () => {
                it('should return the peoples', async () => {
                    let results: PeopleDocument[] = []
                    for (let i: number = 0; i < 10; i++) {
                        const people = await service.create(PeopleDtoMock())
                        // @ts-ignore
                        results.push(people)
                    }
                    const fetchedPeoples = await service.findAll();
                    expect(fetchedPeoples.count).toBe(results.length)
                    fetchedPeoples.results.map((people, index) => {
                        expect(people).toStrictEqual(results[index])
                    })
                })
            })
            describe('and does have pagination', () => {
                it('should return the peoples paginated', async () => {
                    let results: PeopleDocument[] = []
                    for (let i: number = 0; i < 10; i++) {
                        const people = await service.create(PeopleDtoMock())
                        // @ts-ignore
                        results.push(people)
                    }
                    const fetchedPeoples = await service.findAll(4,2);
                    expect(fetchedPeoples.count).toBe(results.length)

                    expect(fetchedPeoples.results[0]).toStrictEqual(results[4])
                    expect(fetchedPeoples.results[1]).toStrictEqual(results[5])
                })
            })
        })
        describe('and the result is 0', () => {
            it('should result in an empty array', async () => {
                const fetchedPeoples = await service.findAll();
                expect(fetchedPeoples.count).toBe(0)
                expect(fetchedPeoples.results).toStrictEqual([])
            })
        })
    })

    /**
     * Teste of findOne service of people
     */
    describe('when getting a people by id', () => {
        describe('and the people is matched', () => {
            it('should return the people', async () => {
                let createdPeople = await service.create(CreatePeopleDtoMock())
                // @ts-ignore
                const fetchedPeople = await service.findOne(createdPeople._id);
                // @ts-ignore
                expect(fetchedPeople).toEqual(createdPeople);
            })
        })
        describe('and the people is not matched', () => {
            it('should throw an error', async () => {
                await expect(service.findOne('63004ec7538879ff3813a8af')).rejects.toThrow();
            })
        })
    })

    /**
     * Teste of update service of people
     */
    describe('when update a people by id', () => {
        describe('and the people is matched', () => {
            it('should return the people updated', async () => {
                let createdPeople = await service.create(CreatePeopleDtoMock())
                // @ts-ignore
                const updatedPeople = await service.update(createdPeople._id, UpdatePeopleDtoMock());
                // @ts-ignore
                expect(updatedPeople.name).toBe(UpdatePeopleDtoMock().name);
                // @ts-ignore
                expect(updatedPeople.birthdate.toISOString().split('T')[0]).toBe(UpdatePeopleDtoMock().birthdate);
                // @ts-ignore
                expect(updatedPeople.gender).toBe(UpdatePeopleDtoMock().gender);
                // @ts-ignore
                expect(updatedPeople.fiscalNumber).toBe(UpdatePeopleDtoMock().fiscalNumber);
            })
        })
        describe('and the people is not matched', () => {
            it('should throw an error', async () => {
                await expect(service.update('63004ec7538879ff3813a8af', UpdatePeopleDtoMock())).rejects.toThrow();
            })
        })
    })

    /**
     * Teste of delete service of people
     */
    describe('when deleting a people by id', () => {
        describe('and the people is matched', () => {
            it('should return the people updated', async () => {
                let createdPeople = await service.create(CreatePeopleDtoMock())
                // @ts-ignore
                await service.delete(createdPeople._id);
                // @ts-ignore
                await expect(service.findOne(createdPeople._id)).rejects.toThrow();
            })
        })
        describe('and the people is not matched', () => {
            it('should throw an error', async () => {
                await expect(service.delete('63004ec7538879ff3813a8af')).rejects.toThrow();
            })
        })
    })
});
