import {IsString, IsDateString, IsNotEmpty, IsArray, ValidateNested, IsOptional} from 'class-validator';
import {Exclude, Type} from "class-transformer";

import CreateContactDto from "../../contacts/dto/create-contact.dto";

export class CreatePeopleDto {
    @IsOptional()
    @Exclude()
    _id?: string;

    @IsString()
    @IsNotEmpty()
    name: string;

    @IsDateString()
    @IsNotEmpty()
    birthdate: string;

    @IsString()
    @IsNotEmpty()
    gender: string;

    @IsString()
    @IsNotEmpty()
    fiscalNumber: string;

    @IsArray()
    @ValidateNested({ each: true })
    @Type(() => CreateContactDto)
    contacts: CreateContactDto[]
}

export default CreatePeopleDto;
