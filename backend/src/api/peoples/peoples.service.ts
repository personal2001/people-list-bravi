import mongoose, {Model} from 'mongoose';
import {Injectable, NotFoundException} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';

import {CreatePeopleDto} from './dto/create-people.dto';
import {UpdatePeopleDto} from './dto/update-people.dto';
import {People, PeopleDocument} from './schema/people.schema';
import {ContactsService} from "../contacts/contacts.service";
import CreateContactDto from "../contacts/dto/create-contact.dto";

@Injectable()
export class PeoplesService {
    constructor(
        @InjectModel(People.name) private peopleModel: Model<PeopleDocument>,
        private readonly contactsService: ContactsService) {
    }

    /**
     *  This function adds a new people
     */
    async create(createPeopleDto: CreatePeopleDto) {
        const session = await this.peopleModel.db.startSession();
        let result
        await session.withTransaction(async () => {
            const people = await new this.peopleModel(createPeopleDto).save({session: session});
            createPeopleDto.contacts.map((contact: CreateContactDto) => {
                contact.people_id = people._id
            })
            const contacts = await this.contactsService.createMany(createPeopleDto.contacts, session);
            result = {...people.toJSON(), contacts}
        });
        await session.endSession();
        return result
    }

    /**
     *  This function returns all peoples
     */
    async findAll(documentsToSkip = 0, limitOfDocuments?: number) {
        const query = this.peopleModel.aggregate([
            {
                $sort: {_id: 1}
            },
            {
              $skip: Number(documentsToSkip)
            },
            {
              $addFields: {contacts:[]}
            },
        ])
        if (limitOfDocuments) {
            query.limit(Number(limitOfDocuments));
        }
        const results = await query;
        const count = await this.peopleModel.count();
        return {results, count};
    }

    /**
     *  `This function returns a id of people`;
     */
    async findOne(id: string) {
        const people = await this.peopleModel.aggregate([
            {
                $match: {_id: new mongoose.Types.ObjectId(id)}
            },
            {
                $lookup: {
                    from: 'contacts',
                    localField: '_id',
                    foreignField: 'people_id',
                    as: 'contacts',
                }
            }
        ]);
        if (people.length <= 0) {
            throw new NotFoundException();
        }
        return people[0]
    }

    /**
     *  `This function updates a id people`;
     */
    async update(id: string, updatePeopleDto: UpdatePeopleDto) {
        const session = await this.peopleModel.db.startSession();
        let result
        await session.withTransaction(async () => {
            let people = await this.peopleModel
                .findByIdAndUpdate(id, updatePeopleDto)
                .setOptions({new: true})
                .session(session);

            if (!people) {
                throw new NotFoundException();
            }


            let contacts: any[] = []
            if (updatePeopleDto.contacts) {
                /**
                 * Update an create contacts
                 */
                const updateContactPromise = updatePeopleDto.contacts.map(async (contact: CreateContactDto) => {
                    let result
                    if (!contact._id) {
                        contact.people_id = id
                        result = await this.contactsService.create(contact, session)
                    } else {
                        const id = contact._id
                        delete contact._id
                        result = await this.contactsService.update(id, contact, session)
                    }
                    contacts.push(result)
                })
                await Promise.all(updateContactPromise)

                /**
                 * Delete contact
                 */
                const fetchedContacts = await this.contactsService.findAll(id, session)
                const deleteContactPromise = fetchedContacts.map(async (contact)=>{
                    const match = contacts.find((value)=> {
                     return String(value._id) == String(contact._id)
                    })
                    if (!match) {
                        await this.contactsService.delete(contact._id, session)
                    }
                })
                await Promise.all(deleteContactPromise)
            }

            result = {...people.toJSON(), contacts}
        });
        await session.endSession();
        return result
    }

    /**
     *  `This function removes a id people`;
     */
    async delete(id: string) {
        const session = await this.peopleModel.db.startSession();
        let result
        await session.withTransaction(async () => {
            result = await this.peopleModel.findByIdAndDelete(id).session(session);
            if (!result) {
                throw new NotFoundException();
            }
            await this.contactsService.deleteAll(id, session)
        })

        return result
    }
}
