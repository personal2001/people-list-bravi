import {Config, names, uniqueNamesGenerator} from 'unique-names-generator';
import mongoose from "mongoose";

import CreateContactDto from "../dto/create-contact.dto";
import {ContactTypes} from "../schema/contact.schema";

export const ContactDtoMock = (people_id: string= new mongoose.Types.ObjectId().toString()): CreateContactDto => {
    const name: Config = {
        dictionaries: [names],
        style: 'capital',
    };

    return {
        type: ContactTypes.email,
        value: `${uniqueNamesGenerator(name)}.${uniqueNamesGenerator(name)}@gmail.com`,
        people_id: people_id,
    };
};