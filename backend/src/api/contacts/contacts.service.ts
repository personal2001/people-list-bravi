import {ClientSession, Model} from 'mongoose';
import {Injectable, NotFoundException} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';

import CreateContactDto from './dto/create-contact.dto';
import {UpdateContactDto} from './dto/update-contact.dto';
import {Contact, ContactDocument} from './schema/contact.schema';

@Injectable()
export class ContactsService {
    constructor(@InjectModel(Contact.name) private contactModel: Model<ContactDocument>) {
    }

    /**
     *  This function adds a new contact to a people
     */
    create(createContactDto: CreateContactDto, session: ClientSession | null = null) {
        const createdContact = new this.contactModel(createContactDto);
        return createdContact.save({session: session});
    }

    /**
     *  This function adds a new contact to a people
     */
    async createMany(createContactDto: CreateContactDto[], session: ClientSession | null = null) {
        return await this.contactModel.insertMany(createContactDto, {session: session});
    }

    /**
     *  This function returns all contacts from a people
     */
    async findAll(people_id: string, session: ClientSession | null = null) {
        return this.contactModel.find({people_id: people_id}).session(session);
    }

    /**
     *  `This function update a concat from a people`;
     */
    async update(id: string, updateContactDto: UpdateContactDto, session: ClientSession | null = null) {
        const contact = await this.contactModel
            .findByIdAndUpdate(id, updateContactDto)
            .setOptions({new: true, upsert: true})
            .session(session);
        if (!contact) {
            throw new NotFoundException();
        }
        return contact
    }

    /**
     *  `This function removes a contact from an people`;
     */
    async delete(id: string, session: ClientSession | null = null) {
        const result = await this.contactModel.findByIdAndDelete(id).session(session);
        if (!result) {
            throw new NotFoundException();
        }
        return result
    }

    /**
     *  `This function removes all contacts from an people`;
     */
    async deleteAll(people_id: string, session: ClientSession | null = null) {
        await this.contactModel.deleteMany({people_id: people_id}).session(session);
        return
    }
}
