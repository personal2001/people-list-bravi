import {IsString, IsNotEmpty, Matches, IsOptional} from 'class-validator';
import {Exclude} from "class-transformer";

import {ContactTypes} from "../schema/contact.schema";

class CreateContactDto {
    @IsOptional()
    @Exclude()
    _id?: string;

    @Matches(`^${Object.values(ContactTypes).filter(v => typeof v !== "number").join('|')}$`, 'i')
    @IsNotEmpty()
    type:ContactTypes

    @IsString()
    @IsNotEmpty()
    value: string

    @IsString()
    @IsOptional()
    people_id: string;
}

export default CreateContactDto;
