import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import mongoose, {Document, ObjectId} from 'mongoose';
import {Transform, Type} from "class-transformer";
import {People} from "../../peoples/schema/people.schema";

export type ContactDocument = Contact & Document

export enum ContactTypes {'email', 'whatsapp', 'phone'}

@Schema({
    timestamps: true,
})
export class Contact {
    @Transform(({value}) => value.toString())
    _id: ObjectId;

    @Prop({type: String, enum: ContactTypes})
    type: string;

    @Prop()
    value: string;

    @Prop({type: mongoose.Schema.Types.ObjectId, ref: 'peoples', required: true})
    @Type(() => People)
    people_id: People;
}

const ContactSchema = SchemaFactory.createForClass(Contact)

ContactSchema.index({people_id: 1, value: 1, type: 1}, {unique: true})

export {ContactSchema};