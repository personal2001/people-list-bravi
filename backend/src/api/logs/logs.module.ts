import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import LogsService from './logs.service';
import { Log, LogsSchema } from './schema/logs.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Log.name, schema: LogsSchema }]),
  ],
  controllers: [],
  providers: [LogsService],
  exports: [LogsService],
})
export default class LogsModule {}
