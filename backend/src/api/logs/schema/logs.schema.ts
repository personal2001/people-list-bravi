import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {Document, ObjectId} from 'mongoose';
import {Transform} from "class-transformer";

export type LogDocument = Log & Document

@Schema({
    timestamps: true,
})
export class Log {
    @Transform(({value}) => value.toString())
    _id: ObjectId;

    @Prop()
    context: string;

    @Prop()
    message: string;

    @Prop()
    level: string;

    @Prop()
    ip: string;

    @Prop()
    method: string;

    @Prop()
    path: string;

    @Prop()
    code: string;

    @Prop()
    status: string;
}

const LogsSchema = SchemaFactory.createForClass(Log)

export {LogsSchema};