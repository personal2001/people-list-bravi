import {IsString, IsNotEmpty} from 'class-validator';

class CreateLogDto {
    @IsString()
    @IsNotEmpty()
    context:string

    @IsString()
    @IsNotEmpty()
    message: string

    @IsString()
    @IsNotEmpty()
    level: string;

    @IsString()
    @IsNotEmpty()
    ip: string;

    @IsString()
    @IsNotEmpty()
    method: string;

    @IsString()
    @IsNotEmpty()
    path: string;

    @IsString()
    @IsNotEmpty()
    code: string;

    @IsString()
    @IsNotEmpty()
    status: string;
}

export default CreateLogDto;
