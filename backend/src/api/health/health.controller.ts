import { Controller, Get } from '@nestjs/common';
import {
  HealthCheckService,
  HealthCheck,
  MongooseHealthIndicator,
  MemoryHealthIndicator,
} from '@nestjs/terminus';

@Controller('health')
class HealthController {
  constructor(
      private healthCheckService: HealthCheckService,
      private mongooseHealthIndicator: MongooseHealthIndicator,
      private memoryHealthIndicator: MemoryHealthIndicator,
  ) {}

  @Get()
  @HealthCheck()
  check() {
    return this.healthCheckService.check([
      () => this.mongooseHealthIndicator.pingCheck('database'),
      // the process should not use more than 500MB memory
      () => this.memoryHealthIndicator.checkHeap('memory heap', 500 * 1024 * 1024),
      // The process should not have more than 500MB RSS memory allocated
      () => this.memoryHealthIndicator.checkRSS('memory RSS', 500 * 1024 * 1024),
    ]);
  }
}

export default HealthController;
