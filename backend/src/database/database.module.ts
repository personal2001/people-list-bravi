import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
    imports: [
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => {
                const username = configService.get('MONGO_USERNAME');
                const password = configService.get('MONGO_PASSWORD');
                const database = configService.get('MONGO_DATABASE');

                const replicaset = configService.get('MONGO_REPLICASET');

                const replicaHost1 = configService.get('MONGO_REPLICA_HOST_1');
                const replicaPort1 = configService.get('MONGO_REPLICA_PORT_1');

                const replicaHost2 = configService.get('MONGO_REPLICA_HOST_2');
                const replicaPort2 = configService.get('MONGO_REPLICA_PORT_2');

                const replicaHost3 = configService.get('MONGO_REPLICA_HOST_3');
                const replicaPort3 = configService.get('MONGO_REPLICA_PORT_3');

                return {
                    uri: `mongodb://${username}:${password}@${replicaHost1}:${replicaPort1},${replicaHost2}:${replicaPort2},${replicaHost3}:${replicaPort3}/${database}?ssl=true&authSource=admin&replicaSet=${replicaset}&retryWrites=true&w=majority`,
                    dbName: database,
                };
            },
            inject: [ConfigService],
        })
    ],
})
export class DatabaseModule {}