#!/bin/bash

sleep 10

echo "Starting replica set initialize"
until mongo  --username admin --password admin --host localhost --port 27018 --eval "print(\"waited for connection\")"
do
    sleep 2
done
echo "Connection finished"
echo "Creating replica set"
#mongo  --username admin --password admin --host localhost --port 27018 <<EOF
#rs.initiate(
#  {
#    _id : 'rs',
#    version: 1,
#    members: [
#      { _id : 0, host : "localhost:27018", "priority": 2 },
#      { _id : 1, host : "localhost:27019", "priority": 0 },
#      { _id : 2, host : "localhost:27020", "priority": 0 },
#    ]
#  }
#)
#rs.status();
#EOF


  {
    _id : 'rs',
    version: 1,
    members: [
      { _id : 0, host : "127.0.0.1:27018", "priority": 2 },
      { _id : 1, host : "127.0.0.1:27019", "priority": 0 },
      { _id : 2, host : "127.0.0.1:27020", "priority": 0 },
    ]
  }

#echo "replica set created"
#
#sleep 10
#
#echo "creating user"
#
#mongo --username admin --password admin --host localhost --port 27018 <<EOF
#   use admin;
#   admin = db.getSiblingDB("admin");
#   admin.createUser(
#     {
#	user: "admin",
#        pwd: "admin",
#        roles: [ { role: "root", db: "admin" } ]
#     });
#     db.getSiblingDB("admin").auth("admin", "admin");
#     rs.status();
#EOF
#
#echo "user created"