FROM node:fermium-alpine as dev

WORKDIR /usr/src/app
COPY ./frontend/package*.json ./

RUN yarn install

COPY ./frontend .

RUN yarn build

RUN yarn start

FROM node:fermium-alpine as prod

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY ./frontend/package*.json ./

RUN yarn install --production=true

COPY . .

EXPOSE 4200

COPY --from=dev /usr/src/app/dist ./dist

RUN yarn start