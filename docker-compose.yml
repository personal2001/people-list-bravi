version: '3.5'

networks:
  mongors-network:

volumes:
  mongo-0.data:
  mongo-1.data:
  mongo-2.data:

services:
  # Definition of three mongo servers that will act as replicas
  mongodb-0.mongo:
    build:
      context: .
      dockerfile: CustomMongo.Dockerfile
    hostname: mongodb-0.mongo
    container_name: mongodb-0.mongo
    ports:
      - 27018:27018
    command: [ "--replSet", "rs", "--bind_ip_all", "--keyFile", "/data/custom_config/mongodb.key", "--journal", "--port", "27018" ]
    networks:
      - mongors-network
    restart: always
    links:
      - mongodb-1.mongo
      - mongodb-2.mongo
    volumes:
      - mongo-0.data:/data/db
    environment:
      MONGO_INITDB_ROOT_USERNAME: ${MONGO_USERNAME}
      MONGO_INITDB_ROOT_PASSWORD: ${MONGO_PASSWORD}
      MONGO_INITDB_DATABASE: ${MONGO_DATABASE}
  mongodb-1.mongo:
    build:
      context: .
      dockerfile: CustomMongo.Dockerfile
    hostname: mongodb-1.mongo
    container_name: mongodb-1.mongo
    ports:
      - 27019:27019
    command: sh -c 'sleep 5 && /usr/local/bin/docker-entrypoint.sh --replSet rs --bind_ip_all --keyFile /data/custom_config/mongodb.key --journal --port 27019'
    networks:
      - mongors-network
    restart: always
    volumes:
      - mongo-1.data:/data/db
    environment:
      MONGO_INITDB_ROOT_USERNAME: ${MONGO_USERNAME}
      MONGO_INITDB_ROOT_PASSWORD: ${MONGO_PASSWORD}
      MONGO_INITDB_DATABASE: ${MONGO_DATABASE}

  mongodb-2.mongo:
    build:
      context: .
      dockerfile: CustomMongo.Dockerfile
    hostname: mongodb-2.mongo
    container_name: mongodb-2.mongo
    ports:
      - 27020:27020
    command: sh -c 'sleep 5 && /usr/local/bin/docker-entrypoint.sh --replSet rs --bind_ip_all --keyFile /data/custom_config/mongodb.key --journal --port 27020'
    networks:
      - mongors-network
    restart: always
    volumes:
      - mongo-2.data:/data/db
    environment:
      MONGO_INITDB_ROOT_USERNAME: ${MONGO_USERNAME}
      MONGO_INITDB_ROOT_PASSWORD: ${MONGO_PASSWORD}
      MONGO_INITDB_DATABASE: ${MONGO_DATABASE}

  # Definition of the initialization server
  # this runs the `rs.initiate` command to initialize
  # the replica set and connect the three servers to each other
  mongo-client:
    image: mongo:latest
    hostname: mongodb-client
    container_name: mongodb-client
    # this container will exit after executing the command
    restart: "no"
    networks:
      mongors-network:
        ipv4_address: 172.22.0.103
    depends_on:
      - mongodb-0.mongo
      - mongodb-1.mongo
      - mongodb-2.mongo
    volumes:
      - ./deployment_scripts:/deployment_scripts
    entrypoint:
      - /deployment_scripts/initiate_replica.sh

  web:
    container_name: "frontend.${NODE_ENV}"
    build:
      context: .
      dockerfile: CustomAngular.Dockerfile
    ports:
      - "4200:4200"
    networks:
      - mongors-network
    restart: unless-stopped
    volumes:
      - ./app-ui:/usr/src/app/app-ui
      - /usr/src/app/app-ui/node_modules

  api:
    container_name: "backend-api.${NODE_ENV}"
    image: "backend-api.${NODE_ENV}"
    environment:
      - NODE_ENV:${NODE_ENV}
    build:
      context: .
      target: "${NODE_ENV}"
      dockerfile: CustomNest.Dockerfile
    entrypoint: [ "npm", "run", "start:${NODE_ENV}" ]
    env_file:
      - .env
    ports:
      - 3001:3001
    networks:
      - mongors-network
    volumes:
      - ./backend:/usr/src/app
      - /usr/src/app/node_modules
    restart: unless-stopped