FROM node:fermium-alpine as dev

WORKDIR /usr/src/app
COPY ./backend/package*.json ./

RUN yarn install

COPY ./backend .

RUN yarn prebuild

RUN yarn build

FROM node:fermium-alpine as prod

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

WORKDIR /usr/src/app

COPY ./backend/package*.json ./

RUN yarn install --production=true

COPY . .

COPY --from=dev /usr/src/app/dist ./dist

CMD ["node", "dist/main"]