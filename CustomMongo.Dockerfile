FROM mongo:latest

RUN cd /
RUN mkdir /data/custom_config/
RUN openssl rand -base64 741 > /data/custom_config/mongodb.key
RUN chmod 600 /data/custom_config/mongodb.key
RUN chown mongodb:mongodb /data/custom_config/mongodb.key