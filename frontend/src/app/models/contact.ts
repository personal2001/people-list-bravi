export interface Contact {
  _id?: string;
  type: string;
  value: string;
  people_id?: string;
}
