import {Contact} from "./contact";

export interface People {
  _id: string;
  name: string;
  birthdate: string;
  gender: string;
  fiscalNumber: string;
  contacts?: Contact[];
}

export interface PeopleList {
  count: number
  results: People[];
}
