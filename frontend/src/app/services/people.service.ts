import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {catchError, Observable, of, retry} from "rxjs";
import {People, PeopleList} from "../models/people";
import {SnackBarService} from "../components/snack-bar/snack-bar.service";
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {
  url = environment.apiURL + '/api/peoples';

  constructor(
    private httpClient: HttpClient,
    private snackBService: SnackBarService,
  ) {
  }

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  }

  getAllPeoples(): Observable<PeopleList> {
    return this.httpClient.get<PeopleList>(this.url)
      .pipe(
        retry(2),
        catchError(this.handleError<PeopleList>('Error GetAll People')))
  }

  getPeopleById(id: string): Observable<People> {
    return this.httpClient.get<People>(this.url + '/' + id)
      .pipe(
        retry(2),
        catchError(this.handleError<People>('Error Get People'))
      )
  }

  createPeople(people: People): Observable<People> {
    return this.httpClient.post<People>(this.url, JSON.stringify(people), this.httpOptions)
      .pipe(
        retry(2),
        catchError(this.handleError<People>('Error Create People'))
      )
  }

  updatePeople(people: People): Observable<People> {
    return this.httpClient.put<People>(this.url + '/' + people._id, JSON.stringify(people), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError<People>('Error Update People'))
      )
  }

  deletePeople(people: People) {
    return this.httpClient.delete<People>(this.url + '/' + people._id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError('Error Delete People'))
      )
  }
  private handleError<T>(message='', operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(`${operation} failed: ${error.message}`);
      this.snackBService.openSnackBar(`${message}: ${error.error.errorMessage}`, 'Okay')
      return of(result as T);
    };
  }
}
