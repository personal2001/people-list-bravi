import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";

import {People} from "../../models/people";
import {PeopleService} from "../../services/people.service";
import {SnackBarService} from "../snack-bar/snack-bar.service";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  people = {} as People;
  peoples: People[] = [];

  constructor(
    private peopleService: PeopleService,
    private router: Router,
    private snackBService: SnackBarService,
  ) {
  }

  ngOnInit() {
    const url = this.router.url.split('/')
    const id = url[url.length - 1]
    if (id !== 'new') {
      this.getPeople(id);
    } else {
      this.people.contacts = []
    }
  }

  getPeople(id: string) {
    this.peopleService.getPeopleById(id).subscribe((people: People) => {
      this.people = people;
      this.people.birthdate = this.people.birthdate.split('T')[0]
    });
  }

  savePeople(form: NgForm) {
    if (this.people._id !== undefined) {
      this.peopleService.updatePeople(this.people).subscribe(async (people: People) => {
        if (people) {
          this.cleanForm(form);
          await this.router.navigate(['/people', 'list'])
          this.snackBService.openSnackBar(`People updated successfully`, 'Okay')
        }
      });
    } else {
      this.peopleService.createPeople(this.people).subscribe(async (people: People) => {
        if (people) {
          this.cleanForm(form);
          await this.router.navigate(['/people', 'list'])
          this.snackBService.openSnackBar(`People created successfully`, 'Okay')
        }
      });
    }
  }

  deletePeople(people: People) {
    this.peopleService.deletePeople(people).subscribe(async () => {
      await this.router.navigate(['/people', 'list'])
      this.snackBService.openSnackBar(`People deleted successfully`, 'Okay')
    });
  }

  editPeople(people: People) {
    this.people = {...people};
  }

  deleteContact(index: number) {
    this.people.contacts?.splice(index, 1);
  }

  addContact() {
    if (this.people.contacts) {
      this.people.contacts?.push({
        type: "",
        value: "",
      })
    } else {
      this.people.contacts = [{
        type: "",
        value: "",
      }]
    }
  }

  cleanForm(form: NgForm) {
    form.resetForm();
    this.people = {} as People;
  }

}
