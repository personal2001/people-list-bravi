import { Component, OnInit } from '@angular/core';
import {People, PeopleList} from "../../models/people";
import {PeopleService} from "../../services/people.service";
import {NgForm} from "@angular/forms";
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  people = {} as People;
  peoples: People[] = [];

  constructor(
    private peopleService: PeopleService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getPeoples();
  }

  getPeoples() {
    this.peopleService.getAllPeoples().subscribe((peoples: PeopleList) => {
      this.peoples = peoples.results;
    });
  }

  savePeople(form: NgForm) {
    if (this.people._id !== undefined) {
      this.peopleService.updatePeople(this.people).subscribe(() => {
        this.cleanForm(form);
      });
    } else {
      this.peopleService.createPeople(this.people).subscribe(() => {
        this.cleanForm(form);
      });
    }
  }

  deletePeople(people: People) {
    this.peopleService.deletePeople(people).subscribe(() => {
      this.getPeoples();
    });
  }

  async editPeople(people: People) {
    await this.router.navigate([`/people/new/${people._id}`]);
  }

  cleanForm(form: NgForm) {
    this.getPeoples();
    form.resetForm();
    this.people = {} as People;
  }

}
