# Frontend

This project was created using [Angular CLI] in version 14.1.3.

### Development server

Run `yarn start` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

### Production server

App was deployed in vercel cloud, to use the production version navigate to `https://people-list-bravi.vercel.app` 

# Backend

This project was created using [Nest CLI] in version 9.0.0.

### Development server

Run `yarn start` for a dev server. Navigate to `http://localhost:3000/`. The application will automatically reload if you change any of the source files.

### Production server

App was deployed in vercel cloud, to use the production version navigate to `https://people-list-bravi-back.vercel.app/`

### Test

Unit and integration test was created for some services in this app. to run the tests run `yarn test` and wait for test suit to finish

# Database

The database choice for this project was mongodb using replicaSet for transactions. 

### Cloud server

The mongo uri for connection is provided by mongo Atlas service. To connect use the default configurations on .env
```bash
NODE_ENV=dev

MONGO_USERNAME=admin
MONGO_PASSWORD=DDiOL3oCgV9aZ1VT
MONGO_DATABASE=bravi

MONGO_REPLICASET=atlas-4pllay-shard-0

MONGO_REPLICA_HOST_1=ac-kzqpfit-shard-00-00.dape6dj.mongodb.net
MONGO_REPLICA_PORT_1=27017
MONGO_REPLICA_HOST_2=ac-kzqpfit-shard-00-01.dape6dj.mongodb.net
MONGO_REPLICA_PORT_2=27017
MONGO_REPLICA_HOST_3=ac-kzqpfit-shard-00-02.dape6dj.mongodb.net
MONGO_REPLICA_PORT_3=27017
```
 
# Docker

The docker containers od this application is crated and orchestrate in the docker-compose file in the root directory.

The composer creates 6 containers to manage and server the full application (front, back, db)

To build and run the container use the command `docker-compose up`

```bash
Mongo db to configure replica sets
mongo-client

Mongo replica sets
mongodb-0.mongo
mongodb-1.mongo
mongodb-2.mongo

Frontend
web

Backend 
api
```
